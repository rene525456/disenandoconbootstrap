Comando para crear un proyecto en nodejs
npm init
npm install lite-server --save-dev

...and add a "script" entry within your project's package.json file:
Inside package.json...
  "scripts": {
    "dev": "lite-server"
  },

git add readme.txt

instalar bootstrap
npm install bootstrap --save
npm install jquery --save
npm install popper.js --save

Instalación de íconos
	npm install open-iconic --save
Agregar la librería iconos
	<link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
	https://useiconic.com/

Ignorar un archivo
gedit .gitignore
.... dentro del archivo agregar lo que se desea ignorar
  node_modules
  
Configurar git  
sudo nano ~/.gitconfig
	[filter "lfs"]
		clean = git-lfs clean -- %f
		smudge = git-lfs smudge -- %f
		process = git-lfs filter-process
		required = true
	[user]
		name = René Guamán-Quinche
		email = rene525456@gmail.com
		password = ]*@2B2@*[
		user = rene525456@gmail.com
  
otra forma
	git config --global user.user rene525456@gmail.com
	git config --global user.email rene525456@gmail.com
	git config --global user.password "]*@2B2@*["
  
Comando en git
	git add .
	git status
	
*************************** Lenguajes para hojas de estilos, sass y less ***************
	
Instalar sass
	npm install node-sass --save-dev
	
	crear una tarea en packace.json
	  "scripts": {
	    "dev": "lite-server",
	    "test": "echo \"Error: no test specified\" && exit 1",
	    "scss": "node-sass -o css css/"
	  },
	  
	crear una archivo
		styles.scss
	
	buscar un color en google "html color picker"
	
	npm run scss
	
Instalar less
	npm install less
	sudo apt install node-less
	lessc css/styles.less css/styles2.css
	
Instalar sincronizar tanto scss y less
	npm install --save-dev onchange rimraf
	
	agregar
	"scripts": {
	    "dev": "lite-server",
	    "test": "echo \"Error: no test specified\" && exit 1",
	    "scss": "node-sass -o css css/",
	    "watch:scss": "onchange 'css/*.scss' --  npm run scss "
	  },
	  
	npm run watch:scss
	
Instalar sincronizar tanto scss y less y el servidor 

	npm install --save-dev onchange concurrently
	  
	agregar
	  "scripts": {
	    "dev": "lite-server",
	    "start": "concurrently \"npm run watch:scss\" \"npm run dev \"",
	    "test": "echo \"Error: no test specified\" && exit 1",
	    "scss": "node-sass -o css css/",
	    "watch:scss": "onchange 'css/*.scss' --  npm run scss "
	  },
	  
	npm run start
	
Para producción

	npm install --save-dev copyfiles
	sudo npm install -g imagemin-cli --unsafe-perm=true --allow-root
	sudo npm install --global imagemin-cli --unsafe-perm=true --allow-root
	npm install --save-dev usemin-cli cssmin uglifyjs htmlmin
	
	"scripts": {
	    "dev": "lite-server",
	    "start": "concurrently \"npm run watch:scss\" \"npm run dev \"",
	    "test": "echo \"Error: no test specified\" && exit 1",
	    "scss": "node-sass -o css css/",
	    "watch:scss": "onchange 'css/*.scss' --  npm run scss ",
	    "clean": "rimraf dist",
	    "imagemin": "imagemin images/* --out-dir dist/images",
	    "usemin": "usemin index.html -d dist --htmlmin -o dist/index.html && usemin about.html -d dist --htmlmin -o dist/about.html && usemin contacto.html -d dist --htmlmin -o dist/contacto.html && usemin precios.html -d dist --htmlmin -o dist/precios.html",
	  },
	
	En cada archivo .html agregar
	
	<!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="with=device-with, initial-scale=1, shrink-to-fit=no">

        <!-- build:css dist/index.css -->
        <link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/styles.css">
        <link rel="stylesheet" href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css">
        <!-- endbuild-->
        
        
        <!-- Optional JavaScript -->
	<!-- jQuery first, then Popper.js, then Bootstrap JS -->
	<!-- build:js dist/index.js -->
        <script src="node_modules/jquery/dist/jquery.min.js"> </script>
        <script src="node_modules/popper.js/dist/umd/popper.min.js"> </script>
        <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"> </script>
        <script src="js/index.js"></script>
        <!-- endbuild -->
        
	Nota: si existe un problema agregar umd (<script src="node_modules/popper.js/dist/umd/popper.min.js"> </script>)
	
	npm run build
	
	npm install grunt --save-dev
	sudo apt install node-grunt-cli
	npm install grunt-contrib-watch --save-dev

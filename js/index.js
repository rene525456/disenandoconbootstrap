$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval:100
    });
    $('#contacto').on('show.bs.modal', function(e){
        console.log('El modal está mostrando');
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('El modal se mostró');
    });
});